#!/bin/bash
# This script launches docker compose for local dev.

ROOT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}")/.. && pwd)
echo "Starting docker compose in $ROOT_DIR."
echo "Press Ctrl-C to end."

docker compose -f "$ROOT_DIR/docker-compose.yml" up --build --force-recreate
